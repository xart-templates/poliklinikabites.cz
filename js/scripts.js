jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	// .mod_custom-intro
	$('.mod_custom-intro').each(function(){
		$('.panes',this).each(function(){
			var i = 0;
			$('.pane',this).each(function(){
				i++;
				$(this).attr({'data-item':'item_0'+i});
			});
		});
		$('.contacts ul',this).each(function(){
			var i = 0;
			$('li',this).each(function(){
				i++;
				$(this).attr({'data-item':'item_0'+i});
				$(this).on('click',function(){
					var item = $(this).data('item');
					$(this).parents('.mod_custom-intro').find('[data-item]').removeClass('on');
					$(this).parents('.item').find('[data-item='+item+']').addClass('on');
				});
			});
			$(document).on('click',function(e){
				if ($(e.target).closest('.mod_custom-intro .contacts li').length === 0) {
					$('.mod_custom-intro [data-item]').removeClass('on');
				}
			});
		});
	});

	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>100){
			$('#gotop').addClass('on');
		} else {
			$('#gotop').removeClass('on');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('#gotop a').click(function(){ 
		$('html,body').animate({scrollTop:0},600);
		return false;
	});

	// file input style
	$('form.normal input[type=file]').each(function(){var userLang=(navigator.language)?navigator.language:navigator.userLanguage;var uploadText='Vyberte soubor';if(userLang=='en'){var uploadText='Choose file'}if(userLang=='de'){var uploadText='Datei ausw&auml;hlen'}if(userLang=='pl'){var uploadText='Wybierz plik'}if(userLang=='fr'){var uploadText='Choisir un fichier'}if(userLang=='ru'){var uploadText='Выберите файл'}if(userLang=='es'){var uploadText='Elegir archivo'}var uploadbutton='<input type="button" class="normal-button" value="'+uploadText+'" />';var inputClass=$(this).attr('class');$(this).wrap('<span class="fileinputs"></span>');$(this).parent().append($('<span class="fakefile" />').append($('<input class="input-file-text" type="text" />').attr({'id':$(this).attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'})).append(uploadbutton));$(this).addClass('type-file').css('opacity',0);$(this).bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake').val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))})});

	if (document.createElement('input').placeholder==undefined){
		// placeholder
		$('[placeholder]').focus(function(){var input=$(this);if(input.val()==input.attr('placeholder')){input.val('');input.removeClass('placeholder')}}).blur(function(){var input=$(this);if(input.val()==''||input.val()==input.attr('placeholder')){input.addClass('placeholder');input.val(input.attr('placeholder'))}}).blur()
	}

});